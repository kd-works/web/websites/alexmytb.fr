<?php

require __DIR__ . '/vendor/autoload.php';
\Sentry\init(['dsn' => 'https://e1606b2b335249a9a491469ede8ee0cb@o378847.ingest.sentry.io/4504776796667904' ]);

require_once __DIR__ . "/classes/controller.php";
$controller = new Controller();

if (isset($_GET["page"])) {
  $title = $controller->setPageTitle($_GET["page"]);
  require __DIR__ . "/pages/" . $_GET["page"] . ".php";
} else {
  $title = " - Accueil";
  require __DIR__ . "/pages/stream.php";
}
