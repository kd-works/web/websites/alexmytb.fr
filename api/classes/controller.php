<?php

/**
 * @package Cadrage
 */
class Controller {
  public function __construct() {}

  ////////////////////////////////////////////////

  public function isLogged() {
    return isset($_SESSION["user"]);
  }

  public function setPageTitle($string) {
    if ($string == 'index') return " - Accueil";
    return " - " . ucfirst($string);
  }
}
