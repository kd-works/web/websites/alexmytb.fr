
    <footer class="page-footer">
      <div class="footer-copyright">
        <div class="container footer-texte">
          © Copyright 2018-<a id="date" class="grey-text"></a> AlexM™️ Corporation | Tous Droits Réservés | <a href="/terms">Termes</a> | <a href="/legal">Mentions légales</a>
          <a class="grey-text text-lighten-4 right">Développement et Design par AlexM &amp; Kasai.</a>
        </div>
      </div>
    </footer>

    <script src="/assets/js/scripts.js"></script>
    <script src="https://browser.sentry-cdn.com/7.40.0/bundle.min.js" integrity="sha384-WUm4t/TlEUBTbZWhvP6vH6K5doLNTpdcQy+Nbsie+eTwsO0ky2vRVLD2aoUr9R+q" crossorigin="anonymous"></script>
<?php if (\Sentry\SentrySdk::getCurrentHub()->getLastEventId()) { ?>
    <script>
      Sentry.init({ dsn: "https://e1606b2b335249a9a491469ede8ee0cb@o378847.ingest.sentry.io/4504776796667904" });
      Sentry.showReportDialog({
        eventId: "<?php echo \Sentry\SentrySdk::getCurrentHub()->getLastEventId(); ?>"
      });
    </script>
<?php } ?>
  </body>
</html>
