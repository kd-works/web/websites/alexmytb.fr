<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site officiel d'AlexM. Stream, vidéos, boutique et informations...">
    <meta name="keywords" content="alexmytb, alexm, ytb, youtube, fortnite, minecraft, tutoriel, esport, streaming, jeu, vidéo">
    
    <title>AlexM<?=$title;?></title>

    <link rel="icon" type="img/x-icon" href="/assets/img/favicon.ico">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="/assets/css/theme.css">
    
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script async src="https://platform.twitter.com/widgets.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  </head>

  <body class="body-content">
    <div class="se-pre-con"></div>
      <?php include __DIR__ . "/navbar.php"; ?>
