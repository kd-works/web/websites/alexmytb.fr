<?php include __DIR__ . "/../content/header2.php";?>

    <div class="container extension-content">
      <div class="row">
        <div class="col l12">
          <h4 class="title-extension">Découvrez mon extension</h4>
          <hr class="hr-extension" />
          <p>Recevez une notification à chaque début de live sur Twitch, restez au courant dès qu'une nouvelle vidéo sort, grâçe à l'extension AlexM™️</p>
        </div>
      </div>

      <div class="row">
        <div class="col m4">
          <button id="chrome" class="btn btn-extension chrome">Ajouter à Chrome<i class="fa fa-chrome left"></i></button>
          <a href="https://chrome.google.com/webstore/detail/alexm/bkfkbbeoabapppnndpfjndpogoklfjcl" target="_blank" class="btn btn-store">Chrome webstore</a>
        </div>

        <div class="col m4">
          <button id="firefox" class="btn btn-extension firefox">Ajouter à Firefox<i class="fa fa-firefox left"></i></button>
          <a href="https://dl.alexmytb.fr/firefox.xpi" target="_blank" class="btn btn-store">Firefox store</a>
        </div>

        <div class="col m4">
          <button id="opera" class="btn btn-extension opera">Ajouter à Opéra<i class="fa fa-opera left"></i></button>
          <a href="https://dl.alexmytb.fr/opera.nex" target="_blank" class="btn btn-store">Opéra store</a>
        </div>

        <!-- <div class="col m4">
          <a href="#" class="btn btn-extension safari not-available">Télécharger pour Safari<i class="fa fa-safari left"></i></a>
        </div> -->

        <!-- <div class="col m4">
          <a href="#" class="btn btn-extension edge not-available">Télécharger pour Edge<i class="fa fa-edge left"></i></a>
        </div> -->
      </div>

      <div class="row fonction-extension">
        <div class="col m8">
          <h4 class="title-extension">Les fonctions de l'extension</h4>
          <hr class="hr-extension-2">
          <p>Découvrer les fonctionnalités de l'extension :</p>

          <ul class="list-fonctions-extension">
            <li>• Un panel avec les réseaux sociaux</li>
            <li>• Un panel avec le nom du jeu, nombres de viewers</li>
            <li>• Un panel avec le nombres d'abonné(e)s en temps réel</li>
            <li>• Notifications au lancement d'un stream</li>
            <li>• Notifications à chaque nouvelle vidéo</li>
          </ul>
        </div>

        <div class="col m6">
          <!-- <img src="https://placehold.it/500x300" class="responsive-img" alt=""> -->
        </div>
      </div>
    </div>

<?php include __DIR__ . "/../content/footer.php";?>
