<?php include __DIR__ . "/../content/header.php";?>

    <div class="container videos-content">
      <div class="row">
        <div class="col l12">
          <h4 class="title-videos">Découvrer mes dernières vidéos</h4>
          <hr class="hr-extension" />
          <p>Retouver les dernières vidéos publier sur ma chaîne YouTube.</p>
        </div>
      </div>

      <div class="row home-videos">
        <div class="col m6">
          <div class="embed-container">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/fPC_AJoWA-Y" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>

        <div class="col m6 pre-videos">
          <h4 class="title-home-video">Toutes les dernières vidéos, vidéos populaires disponible ici et sur ma chaîne YouTube !</h4>
          <p>N'hésite pas à t'abonner pour suivre toutes mes prochaines vidéos.</p>
          <a href="https://www.youtube.com/c/AlexMAW?sub_confirmation=1" target="_blank" class="btn btn-sub-ytb"><i class="fa fa-youtube-play left"></i> S'abonner à la chaîne</a>

          <div class="box-abonnes">
            <div class="subs-api" id="subs"></div>
          </div>
        </div>
      </div>

      <div class="row" style="margin-left: -30px; margin-bottom: 5em;">
        <div class="col m3">
          <a href="https://www.youtube.com/watch?v=hNG2pa0VrKI" target="_blank">
            <div class="card-video">
              <img src="/assets/img/miniatures/minia_skyblock.jpg" class="responsive-img img-video" alt="">
              <h4 class="title-video">TUTORIEL PLUGIN - Crée un SkyBlock sur son serveur minecraft</h4>
            </div>
          </a>
        </div>

        <div class="col m3">
          <a href="https://www.youtube.com/watch?v=CaOb_MdZ30k" target="_blank">
            <div class="card-video">
              <img src="/assets/img/miniatures/minia_noplugin.jpg" class="responsive-img img-video" alt="">
              <h4 class="title-video">TUTORIEL PLUGIN - Cacher les plugins aux joueurs sur son serveur</h4>
            </div>
          </a>
        </div>

        <div class="col m3">
          <a href="https://www.youtube.com/watch?v=fRR0tpK_UO4" target="_blank">
            <div class="card-video">
             <img src="/assets/img/miniatures/minia_f1.jpg" class="responsive-img img-video" alt="">
              <h4 class="title-video">HIGHLIGHTS FORTNITE - En route vers le top 1 ?</h4>
            </div>
          </a>
        </div>

        <div class="col m3">
          <a href="https://www.youtube.com/watch?v=ATIDLF0gIJc" target="_blank">
            <div class="card-video">
              <img src="/assets/img/miniatures/minia_owercraft.jpg" class="responsive-img img-video" alt="">
              <h4 class="title-video">UN SERVEUR INCROYABLE ! - OWERCRAFT (1.9 à 1.12)</h4>
            </div>
          </a>
        </div>
      </div>
    </div>

    <script>
      document.addEventListener('DOMContentLoaded', () => {
        const el = document.getElementById('subs');

        setInterval(async () => {
          await fetch(`https://api.kasai.moe/v1/alexm/count`)
            .then(res => res.json())
            .then(json => el.innerText = json.count)
            .catch(() => el.innerText = 'Error!');
          }, 1e3);
      });
    </script>

<?php include __DIR__ . "/../content/footer.php";?>
