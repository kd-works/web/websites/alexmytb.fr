<?php include __DIR__ . "/../content/header.php";?>

    <div class="container">
      <div class="player-content">
        <div class="content">
          <div class="row">
            <div class="col m9">
              <div class="embed-container">
                <iframe src="https://player.twitch.tv/?channel=alexmytb_&amp;autoplay=true" frameborder="0" scrolling="no" allowfullscreen="true"></iframe>
              </div>	
            </div>

            <div class="col m3">
              <ul id="tabs-swipe-demo" class="chats">
                <li class="tab col m3"><a href="#chat" class="active twitch"><i class="fa fa-twitch" aria-hidden="true"></i></a></li>
                <li class="tab col m3"><a href="#tweets" class="tweets"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li class="tab col m3"><a href="#discord" class="discord"><i class="fa fa-commenting" aria-hidden="true"></i></a></li>
                <li class="tab col m3"><a href="#ts" class="ts"><i class="fa fa-commenting" aria-hidden="true"></i></a></li>
              </ul>

              <div class="chat-container" id="chat">
                <iframe src="https://www.twitch.tv/embed/alexmytb_/chat" frameborder="0" scrolling="no"></iframe>
              </div>

              <div class="chat-container" id="tweets">
                <a class="twitter-timeline" href="https://twitter.com/alexmytb" data-height="720"></a>
              </div>

              <div class="chat-container" id="discord">
                <iframe src="https://discordapp.com/widget?id=405824176235085835" frameborder="0"></iframe>
              </div>

              <div class="chat-container" id="ts">
                <iframe src="https://ts3wi.kasai.works/tsviewpub.php?skey=0&amp;sid=1&amp;showicons=right" frameborder="0" scrolling="yes"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="box-twitch">
          <div class="col m4">
            <a href="https://www.twitch.tv/alexmytb_" target="_blank" class="btn btn-sub btn-box-twitch">S'abonner à la chaîne (Bientôt) <i class="fa fa-check left"></i></a>
          </div>

          <div class="col m4">
            <a href="https://www.twitch.tv/alexmytb_" target="_blank" class="btn btn-follow btn-box-twitch">Suivre la chaîne <i class="fa fa-heart left"></i></a>
          </div>

          <div class="col m4">
            <a href="https://streamlabs.com/alexmaw" target="_blank" class="btn btn-don btn-box-twitch">Faire un don <i class="fa fa-gift left"></i></a>
          </div>
        </div>
      </div>
    </div>

<?php include __DIR__ . "/../content/footer.php";?>
