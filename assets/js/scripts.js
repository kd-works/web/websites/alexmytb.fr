'use strict';

function fade(type, ms, el) {
  var isIn = type === 'in',
    opacity = isIn ? 0 : 1,
    interval = 50,
    duration = ms,
    gap = interval / duration;

  if(isIn) {
    el.style.display = 'inline';
    el.style.opacity = opacity;
  }

  function func() {
    opacity = isIn ? opacity + gap : opacity - gap;
    el.style.opacity = opacity;

    if(opacity <= 0) el.style.display = 'none'
    if(opacity <= 0 || opacity >= 1) window.clearInterval(fading);
  }

  var fading = window.setInterval(func, interval);
}

function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', fn);
  } else {
    document.attachEvent('onreadystatechange', function () {
      if (document.readyState != 'loading')
        fn();
    });
  }
}

if (document.getElementById('chrome')) {
  // https://developer.chrome.com/webstore/inline_installation
  document.getElementById('chrome').addEventListener('click', e => {
    e.preventDefault();
    chrome.webstore.install();
  });

  // https://developer.mozilla.org/en-US/docs/Archive/Add-ons/Installing_Extensions_and_Themes_From_Web_Pages
  document.getElementById('firefox').addEventListener('click', e => {
    e.preventDefault();
    InstallTrigger.install({
      'AlexM™': 'https://dl.alexmytb.fr/firefox.xpi'
    });
  });

  // https://dev.opera.com/extensions/inline-installation
  document.getElementById('opera').addEventListener('click', e => {
    e.preventDefault();
    opr.addons.installExtension('bjjbfbcolocnfcgeeggiadiadidoiinj', console.log, console.error);
  })
}

ready(() => {
  $('ul.chats').tabs();

  document.getElementById('date').innerText = new Date().getFullYear();

  window.addEventListener('load', () => {
    fade('out', 500, document.getElementsByClassName('se-pre-con')[0]);

    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-9967916164253518",
      enable_page_level_ads: true
    });
  });
});
